
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import source.Lab0;
import java.lang.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class lab0_test {
    Lab0 lab = new Lab0();

    ////////////////////////////////////

    @Test(dataProvider = "begin")
    public void beginTest(float vol, float sqr,float hgt, Lab0.VolandSquare res)
    {
        assertEquals(lab.mBegin(vol,sqr,hgt),res);

    }

    @DataProvider
    public Object[][] begin()
    {
        return new Object[][]{{1,2,3,new Lab0.VolandSquare(6,22)},{2,3,4,new Lab0.VolandSquare(24,52)}};
    }


    @Test(expectedExceptions = AssertionError.class, dataProvider = "negativeCuboidVTest")
    public void negativeCuboidVTest(float length,float width,float height)
    {
        lab.mBegin(length,width,height);
    }

    @DataProvider
    public Object[][] negativeCuboidVTest()
    {
        return new Object[][]{{-1, 2,1}, {-2, 1,-3}, {2, -1,-1}};
    }

    //////////////////////////////////////

    @Test(dataProvider = "inttest")
    public void IntegerTest(int a, Lab0.TwoDigitNumber res)
    {
        assertEquals(lab.mInteger(a),res);
    }
    @DataProvider
    public Object[][] inttest()
    {
        return new Object[][]{{24,new Lab0.TwoDigitNumber(2,4)},{13,new Lab0.TwoDigitNumber(1,3)}};
    }




    @Test(expectedExceptions = AssertionError.class, dataProvider = "IntegerETest")
    public void IntegerETest(int num)
    {
        lab.mInteger(num);
    }
    @DataProvider
    public Object[][] IntegerETest()
    {
        return new Object[][]{{-1}, {9}, {100}};
    }

    ///////////////////////////////////////

    @Test(dataProvider = "BooleanData")
    public void BoolTest(int a,int b,int c,boolean res)
    {
        assertEquals(lab.mBoolean(a,b,c),res);
    }
    @DataProvider
    public Object[][] BooleanData()
    {
        return new Object[][]{{1,2,3,true},{3,2,1,false}};

    }

    //////////////////////////////////////

    @Test(dataProvider = "ifTest")
    public void IFTest(float a,float b,float result)
    {
        assertEquals(lab.mIf(a,b),result);
    }
    @DataProvider
    public Object[][] ifTest()
    {
        return new Object[][]{{1,2,2},{1,100,100},{-1,-2,-1} };
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "ifExpTest")
    public void IFexpTest(float a,float b)
    {
        lab.mIf(a,b);
    }
    @DataProvider
    public Object[][] ifExpTest()
    {
        return  new Object[][]{ {1,1},{0,0}};
    }

    /////////////////////////////////////

    @Test(dataProvider = "CaseTest")
    public void caseTest(int k,float value,float result)
    {
        assertEquals(lab.mCase(k,value),result);
    }
    @DataProvider
    public Object[][] CaseTest()
    {
        return  new Object[][]{ {3,10,10},{1,100,10}};
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "CaseExpTest")
    public void caseExpTest(int k,float value)
    {
        lab.mCase(k,value);
    }
    @DataProvider
    public  Object[][] CaseExpTest()
    {
        return new Object[][]{{6,10},{0,5}};
    }

    ///////////////////////////////////////

    @Test(dataProvider = "FibonachiTest")
    public void FibTest(int n, int[] arr)
    {
        assertEquals(lab.mFibonachi(n),arr);
    }
    @DataProvider
    public Object[][] FibonachiTest()
    {
        int [] a = {1,1},b = {1,1,2,3,5};
        return new Object[][]{ {2,a}, {5,b}};
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "FibExpTest")
    public void fibExpTest(int n)
    {
        lab.mFibonachi(n);
    }
    @DataProvider
    public Object[][] FibExpTest()
    {
        return  new Object[][]{ {1},{0},{-1}};
    }

    ///////////////////////////////////////

    @Test(dataProvider = "WhileTest")
    public void whileTest(int n,float result)
    {
        assertEquals(lab.mWhile(n),result);
    }
    @DataProvider
    public Object[][] WhileTest()
    {
        return new Object[][]{{5,15},{8,384}};
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "WhileExpTest")
    public void whileExpTest(int n)
    {
        lab.mWhile(n);
    }

    @DataProvider
    public Object[][] WhileExpTest ()
    {
        return new Object[][]{ {-1},{0}};
    }

    /////////////////////////////////////

    @Test(dataProvider = "ArrayTest")
    public void arrTest(int[] arr, int k, int l, double result)
    {
        assertEquals(lab.mArray(arr,k,l),result);
    }
    @DataProvider
    public Object[][] ArrayTest()
    {
        int[] a = {5,23,31,43,1,0,3};
        double r = 7.75;
        return new Object[][]{{a,3,6,r}};
    }


    @Test(expectedExceptions = AssertionError.class, dataProvider = "ArrayExpTest")
    public void arrExpTest(int[] arr, int k, int l)
    {
        lab.mArray(arr,k,l);
    }

    @DataProvider
    public Object[][] ArrayExpTest()
    {
        int[] a = {5,23,31,43,1,0,3};
        return  new Object[][]{ {a,0,a.length}};
    }

    /////////////////////////////////////

    @Test(dataProvider = "MatrixTestData")
    public void matrixTest(int[][] mat,int[][] result)
    {
        //Assert.assertArrayEquals(lab.matrix(mat),result);
        //assertThat(lab.matrix(mat), result);
        //List<List<Integer>> listOfLists = new ArrayList<List<Integer>>();
        //System.out.println("My  output: "+ lab.matrix(mat)[0][0]);
        assertTrue(equals(lab.matrix(mat),result));


    }
// equals  overloading for comparing matrix
    private boolean equals(int[][] matrix, int[][] result) {
        if(matrix.length!=result.length)return false;
        else
        {

            int N = matrix.length;
            int M = matrix[0].length;
            boolean res;
            for( int i=0; i < N;i++)
            {

                for( int j=0; j < M ; j++)
                {
                    if(matrix[i][j]!= result[i][j])
                    {
                        return false;


                    }
                }

            }
        }
        return true;
    }

    @DataProvider
    public Object[][] MatrixTestData()
    {
        int [][] matinput = {{1,2,3},{4,5,6}};
        int [][] matout = { {3,2,1} ,{ 6,5,4 }};
       //System.out.println("My  output: "+ matinput[0][0]);
        return new Object[][]{{matinput,matout}} ;
    }



}


