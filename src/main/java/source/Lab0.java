package source;

import java.util.List;
import java.util.Objects;

public class Lab0 {

    /**
     * class of Volume and Square
     */
    public static class VolandSquare
    {

        private float volume;
        private float square;

        public float getSquare()
        {
            return square;
        }

        public void setSquare(float square) {
            this.square = square;
        }


        public VolandSquare(float vol, float sqr)
        {
            volume = vol;
            square = sqr;
        }

        public float getVolume() {
            return volume;
        }

        public void setVolume(float volume) {
            this.volume = volume;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof VolandSquare)) return false;
            VolandSquare that = (VolandSquare) o;
            return Float.compare(that.volume, volume) == 0 &&
                    Float.compare(that.square, square) == 0;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(volume, square);
        }
    }


    /**
     * class of two digits of twosigns-number
     */
    public static class TwoDigitNumber{
        private int firstDigit = 0;
        private int secondDigit = 0;

        public TwoDigitNumber(int a,int b)
        {
            firstDigit =a;
            secondDigit =b;
        }

        public int getFirstDigit()
        {
            return firstDigit;
        }

        public void setFirstDigit(int firstDigit)
        {
            this.firstDigit = firstDigit;
        }

        public int getSecondDigit()
        {
            return secondDigit;
        }

        public void setSecondDigit(int secondDigit)
        {
            this.secondDigit = secondDigit;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof TwoDigitNumber)) return false;
            TwoDigitNumber that = (TwoDigitNumber) o;
            return firstDigit == that.firstDigit &&
                    secondDigit == that.secondDigit;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(firstDigit, secondDigit);
        }
    }




    /**
     * @param length of cuboid
     * @param width of cuboid
     * @param height of cuboid
     * @return   Volume of cuboid
     */

    public VolandSquare mBegin(float length, float width, float height)
    {
        //System.out.println("The  volume of cuboid is "+ a*b*c+ "\t");
        assert (length > 0 && width > 0 && height > 0) : "Arguments should be more than zero";
        return new VolandSquare(length*width*height,2*(length*width+ width*height+ length*height)) ;
    }


    /**
     * @param num is a twodigits-number
     * @return a object TwoDigitNumber with a first and second digit of number
     */
    public  TwoDigitNumber  mInteger(int num)
    {
        assert (num>9 && num <100) : "Your number has to be between 10 and 99";
        return new TwoDigitNumber(num/10,num%10);
    }



    /**
     * this method returns True if A<B<C
     * @param A is  a number
     * @param B is  a number
     * @param C is  a number
     * @return value of  inequality A<B<C
     */
    public  boolean mBoolean(int A,int B, int C)
    {
        return (A < B & B < C);
    }


    /**
     * @param first is a first number
     * @param second is a second number
     * @return bigger number
     */
    public  float mIf(float first, float second)
    {
        assert (first!=second) : "Your numbers have to be different";
        if(first >second )return first;
        return  second;
    }


    /**
     *
     * @param k is a type of  measurment
     * @param num is a value of measurment
     * @return value converted to the centimetres
     */
    public float mCase(int k,float num) {
        assert (k >=1 && k<=5): "Your type of measurement has to be a digit between 1-5";
        System.out.println("k = "+ k + " num = "+ num);
        float result=0;
        switch (k)
        {
            case 1: result= num/10;
            break;
            case 2: result =  num*1000;
            break;
            case 3: result=  num;
            break;
            case 4: result =  num/1000;
            break;
            case 5: result= num/100;
                break;
        }
        return result;

    }

    /**
     * @param n is number of the biggest Fibonachu element
     * @return Fibonachi array
     */
    public  int[] mFibonachi(int n) {
        assert (n>1): "Your Integer has to be positive and bigger than 1 ";

        int arr[] = new int[n];
        for(int i=0;i<n;i++)
        {
            arr[i]=fib(i+1,arr);
        }
        return arr;

    }




    /**
     * calculate fib element
     * @param n is a number of element
     * @return fibonachi value
     */
    public int fib(int n,int [] arr) {
        if (n <= 2) return 1;
        return  arr[n-2]+ arr[n-3];
    }

    /**
     * @param n is number of array elements
     * @return the double factorial of element
     */
    public float mWhile(int n) {
        assert (n>0): "Your integer has to be strongly positive";
        float f=1;
        while(n>=1) {
            f *= n;
            n -= 2;
        }
        return f;
    }


    /**
     *
     * @param arr is a array of Integer
     * @param K is a left bound of non-calculating elements
     * @param L is a right bound of non-calculating elements
     * @return float arithmetic mean
     */

    public double mArray(int []arr,int K,int L)
    {
        int N = arr.length;
        int num=0;
        //System.out.println("The length of array :"+ arr.length);
        double sum =0,res=0;
        assert (K>1 && L>=K && N>=L): "Your numbers have to satisfy 1<k<=L<=N";
        for(int i=0; i<N; i++)
        {
            if(i< K-1 || i> L-2)
            {
                sum+=arr[i];
                num++;
                // System.out.println(i +" "+ arr[i]+ " "+ num);
            }
        }
        res = sum/num;
        //System.out.println("The result is :"+ res);
        return res;
    }



    /**
     * This method change columns of matrix with minimum and maximum element
     * @param mat is a matrix
     * @return matrix with changed colums that includes maximum and minimum velues
     */
    public int[][] matrix(int[][] mat)
    {
        int max = 0, min = 0;
        int mine = 0, maxe = 0 ;
        int N = mat.length;
        int M = mat[0].length;
        int temp = 0;

        mine = mat[0][0];
        maxe = mat[0][0];

        for( int i=0; i < N;i++)
        {

            for( int j=0; j < M ; j++)
            {
                if(mat[i][j]< mine)
                {
                    mine = mat[i][j];
                    min = j;


                }
                if(mat[i][j] > maxe)
                {
                    maxe = mat[i][j];
                    max = j;
                }
                //System.out.println(i + " " + j);

            }

        }
        //System.out.println("The (max; min) is ( "+ max+ " ; "+ min);
        for(int i=0; i < N; i++)
        {
            temp = mat[i][min];
            mat[i][min] = mat[i][max];
            mat[i][max] = temp;

        }


        return mat;


    }



}
